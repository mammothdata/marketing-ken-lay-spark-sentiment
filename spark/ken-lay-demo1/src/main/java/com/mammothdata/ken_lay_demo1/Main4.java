package com.mammothdata.ken_lay_demo1;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.MimeConfig;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class Main4 
{

	public static void main(String[] args) 
	{
		
		final HashMap<String, Integer> afinnLexicon = new HashMap<String,Integer>();
		
		 String inputDir = "/home/prhodes/development/experimental/search/lucene/enron_tiny/collapsed";
		 SparkConf conf = new SparkConf().setAppName("Simple Application");
		 JavaSparkContext sc = new JavaSparkContext(conf);
		 
		 try
		 {
			 JavaRDD<String> afinnData = sc.textFile( "/home/prhodes/development/projects/mammoth/ken-lay/spark/AFINN/AFINN-111.txt" );
			 List<String> afinnEntries = afinnData.collect();
			 for( String afinnEntry : afinnEntries )
			 {
				 String[] entryParts = afinnEntry.split( "\\s+");
				 
				 // we do this to skip multi-word phrases, like "can't stand" and "cashing in"
				 if( entryParts.length == 2 )
				 {
					 String word = entryParts[0];
					 Integer valence = Integer.parseInt(entryParts[1]);
				 
					 afinnLexicon.put(word, valence);
				 }
			 }
			 
			 
			 JavaPairRDD<String, String> inputData = sc.wholeTextFiles(inputDir);
			 
			 
			 JavaPairRDD<String, Map<String,Integer>> dataWithSentimentScores = inputData.mapToPair( new PairFunction< Tuple2<String,String>, String, Map<String,Integer>>() {
				 
				 public Tuple2<String, Map<String,Integer>> call(Tuple2<String, String> input ) throws Exception {
					
					  String filename = input._1();
					  String content = input._2();
					 
					  
					  // parse email
					  EmailContentHandler handler = new EmailContentHandler();
					  MimeConfig config = new MimeConfig();
					  config.setMaxLineLen(-1);
					  config.setMaxHeaderLen(-1);
					  config.setMaxContentLen(-1);
					  config.setMaxHeaderCount(-1);
		              MimeStreamParser parser = new MimeStreamParser(config);
		              parser.setContentHandler( handler );
		              ByteArrayInputStream bais = new ByteArrayInputStream( content.getBytes() );
		              parser.parse( new BufferedInputStream( bais ) );					  
					  
		              String emailBody = handler.get( EmailContentHandler.BODY );
		              String sender = handler.get( EmailContentHandler.HEADER_FROM );
		              if( !(sender.contains( "@enron.com" )))
		              {
		            	  sender = "ignore";
		              }
		              
		              String addressee = null;
		              String toString = handler.get( EmailContentHandler.HEADER_TO );
		              if( toString == null )
		              {
		            	  System.err.println( "no TO: in file: " + filename );
		            	  sender = "ignore";
		              }
		              else
		              {
			              // as a convenience here, we're only going to deal with cleanly formatted
			              // email address in the form "username@enron.com".  We also only deal with
			              // emails to other @enron.com addresses.  Also, we ignore emails to
			              // groups (that is, more than one entry in the "TO:" field.
			              String[] toParts = toString.split( ",");
			              
			              if( toParts.length == 1 )
			              {
			            	  String to = toParts[0];
			            	  if( to.contains( "@enron.com" ))
			            	  {
			            		  addressee = to;
			            	  }
			            	  
			              }
		              }
		              
		              // output the sentiment score as (key = sender, value = <addressee,score>)
		              Tuple2<String,Map<String,Integer>> output = new Tuple2<String,Map<String,Integer>>( sender, null );

		              if( addressee != null )
		              {
		            	  // calculate sentiment score
		            	  int sentimentScore = 0; // start with a score of 0 for perfectly neutral
		            	  
		            	  String[] emailBodyTokens = emailBody.split( "\\s+" );
		            	  for( String emailBodyToken : emailBodyTokens )
		            	  {
		            		  if( afinnLexicon.containsKey(emailBodyToken))
		            		  {
		            			  int valence = afinnLexicon.get(emailBodyToken);
		            			  sentimentScore = sentimentScore + valence;
		            		  }
		            	  }
		            	  
		            	  Map<String,Integer> scoreMapping = new HashMap<String,Integer>();
		            	  scoreMapping.put(addressee, sentimentScore);
		            	  
		            	  output = new Tuple2<String,Map<String,Integer>>( sender, scoreMapping );
		              }
		              					  
					  
		              return output;
				}
			 });
			 
			 // filter out data we don't want
			 JavaPairRDD<String, Map<String, Integer>> filteredResults = dataWithSentimentScores.filter( new Function<Tuple2<String,Map<String,Integer>>,Boolean>() {

				public Boolean call(Tuple2<String, Map<String, Integer>> arg0) throws Exception 
				{
					if( arg0._1.equals("ignore") || arg0._2 == null )
					{
						return false;
					}
					else
					{
						return true;
					}
				}
			 });
			 
			
			 JavaPairRDD<String, Iterable<Map<String, Integer>>> groupedScoresResults = filteredResults.groupByKey();
			 
			 JavaRDD<Map<String, Map<String, Integer>>> summedScoresResults = groupedScoresResults.map( new Function< Tuple2 < String, Iterable<Map<String,Integer>>>, Map<String, Map<String,Integer>>>() {

				public Map<String, Map<String,  Integer> > call(Tuple2<String, Iterable<Map<String, Integer>>> arg0) throws Exception {
					
					String key = arg0._1;
					Iterable<Map<String, Integer>> scores = arg0._2;
					
					// we have a list of Maps, where each map contains addressee <-> score mappings for the
					// sender (as represented by the key).  We need to flatten this structure and sum the
					// score for a given addressee for this sender.  
					Map<String, Map<String,Integer>> output = new HashMap<String, Map<String,Integer>>();
					
					Map<String, Integer> intermediateMap = new HashMap<String, Integer>();
					Map<String, Integer> addresseeCount = new HashMap<String,Integer>();
					
					for( Map<String,Integer> scoreMap : scores )
					{
						Set<Map.Entry<String, Integer>> entries = scoreMap.entrySet();
					
						for( Map.Entry<String, Integer> entry : entries )
						{
							String addressee = entry.getKey();
							Integer score = entry.getValue();
							
							if( intermediateMap.containsKey(addressee ))
							{
								// we've already inserted a score for this addressee, so retrieve the
								// current value and add the score from this entry (note: the score may be
								// negative, in which case "adding" is really subtracting)
								int currentAggregatedScore = intermediateMap.get(addressee);
								currentAggregatedScore = currentAggregatedScore + score;
								
								intermediateMap.remove(addressee);
								intermediateMap.put(addressee, currentAggregatedScore);
									
								int currentAddresseeCount = addresseeCount.get(addressee);
								currentAddresseeCount++;
								addresseeCount.remove(addressee);
								addresseeCount.put( addressee, currentAddresseeCount );
								
							}
							else
							{
								intermediateMap.put(addressee, score);
								addresseeCount.put( addressee, 1);
							}
							
						}
					}
					
					// divide the raw scores by the number of entries for that addressee to normalize the
					// final number.  In theory, both numbers, the raw total and the average, could be useful
					// as the "affinity" a user has for another user may affect how frequently they send emails
					// to that user. 
					
					// copy the keys into a separate set to avoid
					// ConcurrentModificationException
					Set<String> keys = new HashSet<String>();
					keys.addAll(intermediateMap.keySet());
					
					for( String k : keys )
					{
						int score = intermediateMap.get(k);
						int count = addresseeCount.get(k);
						
						// we'll just ignore the floating point part of this for now
						// as imprecise as this scoring is to begin with, a few decimal points
						// aren't likely to affect the way the final number is used.
						int normalizedScore = score / count;
						
						intermediateMap.remove(k);
						intermediateMap.put(k, normalizedScore);
					}					
					
					
					output.put(key, intermediateMap );
					
					return output;
				}} );
			 
			 
			  
			 // we now collect() the results so we can flatten the nested structure of the data and write it
			 // out as a CSV file that we can load into Hive.
			 List<Map<String, Map<String, Integer>>> collectedResults = summedScoresResults.collect();
			 
			 File outputFile = null;
			 FileWriter outputWriter = null;
			 try
			 {
				 
				 outputFile = new File( "/home/prhodes/development/projects/mammoth/ken-lay/spark/output.txt" );
				 outputWriter = new FileWriter( outputFile );
				 
				 				 
				 // final PreparedStatement st = dbConn.prepareStatement( "insert into sentiment_mapping values( ?, ?, ? )" );

				 
				 for( Map<String, Map<String,Integer>> arg0 : collectedResults )
				 {
				 	
					Set<Entry<String, Map<String, Integer>>> entries = arg0.entrySet();
					
					for( Entry<String, Map<String, Integer>> entry : entries )
					{
						String sender = entry.getKey();
						Map<String,Integer> scores = entry.getValue();

						Set<Entry<String,Integer>> scoreEntries = scores.entrySet();
						for( Entry<String,Integer> scoreEntry : scoreEntries )
						{
							String addressee = scoreEntry.getKey();
							int score = scoreEntry.getValue();
							
							// st.setString(1, sender);
							// st.setString(2, addressee);
							// st.setInt(3, score);
							// st.executeUpdate();
							String outputLine = sender + "," + addressee + "," + score + "\n";
							outputWriter.write(outputLine);
						}
						
					}

				 }
				 
				 outputWriter.write( "\n" );
			 }
			 catch( Exception e)
			 {
				 e.printStackTrace();
			 }
			 finally
			 {
				 if( outputWriter != null )
				 {
					 try {
						outputWriter.close(); 
					 }
					 catch(Exception e )
					 {}
				 }
			 }
		 }
		 finally
		 {
			 sc.close();
		 }
		 
		 
	}

}
