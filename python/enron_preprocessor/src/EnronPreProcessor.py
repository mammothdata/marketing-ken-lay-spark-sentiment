'''
Created on Feb 17, 2016

@author: prhodes
'''

import os
import sys

if __name__ == '__main__':
    
    print( "Hello, Enron world!\n")
    
    # recursively iterate through the maildir tree
    # and process each message.  For each message, write it
    # out as <MessageID>.txt where the message id is taken
    # from the email header
    
    count = 0
    errorCount = 0
    errors = {}
    
    # '/home/prhodes/development/experimental/search/lucene/enron_tiny/corpus/maildir'
    for folder, subs, files in os.walk( sys.argv[1] ):
        
        print( "Folder is : " + folder + ", with " + str( len(files) ) + " files.")
        
        for filename in files:
            
            count = count+1
            # print( "Folder is: " + folder + ", Filename is: " + filename )
            path = os.path.join(folder, filename)
            print( "Path is: " + path )
            
            currentFile = None
            try:
                
                currentFile = open( path, 'r', encoding="cp1252")
                
                # loop over lines until we find the "Message-ID:" line (it will usually be first, but we
                # might not want to count on that.
                # once we have the message id, write this same file back out to a new file named
                # <MessageID>.txt in our destination directory
                
                # print( currentFile.readline())
                
                for line in currentFile:
                    if( "Message-ID:" in line ):
                        
                        print( "Message-ID: found!" )
                        
                        # parse the line to get just the ID part.
                        idParts = line.lstrip( "Message-ID:").split(".")[0:2]   
                        messageID = "".join(idParts) 
                        strippedMessageID = messageID.replace( "<", "" ).replace( " ", "" )
                        
                        # "/home/prhodes/development/experimental/search/lucene/enron_tiny/collapsed"
                        outputFilePath = os.path.join( sys.argv[2], strippedMessageID + ".txt")
                        
                        outFile = open( outputFilePath, "w" )
                        currentFile.seek(0)
                        content = currentFile.read()
                        outFile.write( content )
                        
                        outFile.close()
                        currentFile.close()
                        
                        break
                        
                    else:
                        print( "Nothing doing..." )
                        continue
                
                currentFile.close()    
                
                
            except Exception as err:
                
                errors[path] = format(err)
                
                print("Error: {0}".format(err))
                errorCount = errorCount + 1
                if( currentFile != None ):
                    currentFile.close()
                
                
    print( str( count ) + " files processed" )
    print( str( errorCount ) + " errors encountered" )
    
    for error in errors:
        print( errors[error] )
        
    