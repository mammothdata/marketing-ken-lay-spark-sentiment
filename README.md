# Who Does Ken Lay Like Most - Sentiment Analysis with Spark

This repo contains the following:

* Spark Java code for processing the Enron email corpus and performing Sentiment Analysis computation on each email.   The output is a
mapping from each sender to each addressee, with a normalized total "sentiment score".   Only emails between @enron.com domain addresses are
used.

* Python code for pre-processing the maildir structure and collapsing the messages into a single directory, with each file named <message-id>.txt.

* A BIRT report definition for creating a nicely formatted report from the results (when stored in Postgres).


The Main2.java program performs the SA calculation and stores the results as a text file.   The
Main3.java program performs the same calculations but stores the results in a Postgres database
enabling the use of conventional BI tools like BIRT or Tableau for reporting.

